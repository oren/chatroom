# chatroom


## Assignment
Here is your Golang challenge:
The main objective is to learn Golang.
Here is a relatively simple exercise to walk you through it:

Write a simple CLI real-time chat (single room) with a client-server design.

The goal of the chat is to be able to send one to many msgs.

Guidelines:
1. It should be possible to open at least 4 CMDs from different hosts and write a message, each message will be immediately visible to other users (text messages).
2. Use go-routines.
3. Use channels.
4. Don't overuse go-routines or channels, keep it effective and efficient

You will be measured by the code's quality, time, efficiency, and how thoroughly you know Golang.

If something in the challenge is not clear enough or you have any questions, feel free to contact me.

Good luck :)
