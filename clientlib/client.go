package client

import (
  "net"
  "bufio"
  "fmt"
  "os"
  "gitlab.com/oren/chatroom/commonlib"
)

func Start(username string, ip string) {
  fmt.Println("Starting Client:")
  fmt.Println("- Username:", username)
  fmt.Println("- IP:", ip)

  conn, err := net.Dial(common.PROTOCOL, ip + ":" + common.PORT)
  common.AssertError("dialing " + ip, err)
  go listen(conn)

  _, err = fmt.Fprintf(conn, username + "\n")
  common.AssertError("writing username to conn", err)

  scanner := bufio.NewScanner(os.Stdin)
  for scanner.Scan() {
    _, err := fmt.Fprintf(conn, scanner.Text() + "\n")
    common.AssertError("writing to conn", err)
  }
  common.AssertError("reading from stdin", scanner.Err())
}

func listen(conn net.Conn) {
  scanner := bufio.NewScanner(conn)
  for scanner.Scan() {
    _, err := fmt.Fprintln(os.Stdout, scanner.Text())
    common.AssertError("writing to stdout", err)
  }
  common.AssertError("reading from conn", scanner.Err())
}

