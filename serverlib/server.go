package server

import (
  "bufio"
  "fmt"
  "io"
  "net"
  "strings"
  "gitlab.com/oren/chatroom/commonlib"
)

type message struct {
  addr net.Addr
  text string
}

var connections = make(map[net.Addr] net.Conn)
var incomingMessages = make(chan message)


func Start() {
  fmt.Println("Starting Server")
  listener, err := net.Listen(common.PROTOCOL, ":" + common.PORT)
  common.AssertError("listening on port " + common.PORT, err)
  go sender()
  for {
    conn, err := listener.Accept()
    common.CheckError("accepting connection" , err)
    connections[conn.RemoteAddr()] = conn
    go handleConnection(conn)
  }
}

func handleConnection(conn net.Conn) {
  // This is the routine that watches the connection and gets the text.
  // First line is always username, and it will be attached to every mesage coming from this connection
  reader := bufio.NewReader(conn)
  username, err := reader.ReadString('\n')
  common.CheckError("getting username" , err)
  username = strings.Trim(username, "\n")
  addr := conn.RemoteAddr()

  for {
    text, err := reader.ReadString('\n')
    if err == io.EOF {
      return
    }
    common.CheckError("reading message" , err)
    text = username + ": " + text
    newMessage := message{addr: addr, text: text}
    incomingMessages <- newMessage
  }
}

func sender() {
  // Will listen for new messages on channel incomingMessages and distribute it on all connection,
  //   aside from the sender's one
  for {
    newMessage := <-incomingMessages
    for addr, conn := range connections {
      if newMessage.addr != addr {
        _, err := fmt.Fprintf(conn, newMessage.text)
        common.CheckError("writing to conn: " + addr.String(), err)
        if err != nil {
          delete(connections, addr)
        }
      }
    }
  }
}
