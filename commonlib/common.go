package common

import (
  "fmt"
  "os"
)

const (
  PROTOCOL = "tcp"
  PORT = "8089"
)

// For recoverable errors
func CheckError(context string, err error) {
  if err != nil {
    fmt.Fprintln(os.Stderr, "Error in " + context + ":", err)
  }
}

// For unrecoverable errors. On error will exit!
func AssertError(context string, err error) {
  if err != nil {
    fmt.Fprintln(os.Stderr, "Error in " + context + ":", err)
    os.Exit(1)
  }
}

