package main

import (
  "flag"
  "fmt"
  "os"
  "gitlab.com/oren/chatroom/serverlib"
  "gitlab.com/oren/chatroom/clientlib"
)

func main() {
  serverCommand := flag.NewFlagSet("server", flag.ExitOnError)
  clientCommand := flag.NewFlagSet("client", flag.ExitOnError)

  if len(os.Args) < 2 {
    usage()
  }

  switch os.Args[1] {

  case "server":
    serverCommand.Parse(os.Args[2:])
    server.Start()
  case "client":
    clientCommand.Parse(os.Args[2:])
    args := clientCommand.Args()
    if len(args) < 2 {
      usage()
    }
    client.Start(args[0], args[1])
  default:
    usage()
  }
}

func usage() {
  fmt.Println("expected Commands:")
  fmt.Println(os.Args[0], " server")
  fmt.Println(os.Args[0], " client <name> <server-ip>")
  os.Exit(1)
}

